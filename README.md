- Install the postgres (on machine or at a docker instance);
- Make sure that the password of postgres user is 123456;
- Create database tasks;
- Use the 8.12.0 node version;
- Execute the npm install command at the project directory;
- To create the tables at database, execute the following command: knex migrate:latest;
- Start the application with the npm start command.


Optional: command to start BD with docker <br/>
docker-compose -f src/main/docker/postgresql.yml up -d